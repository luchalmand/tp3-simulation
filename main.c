#include "mt19937ar.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * @brief              Function to estimate Pi using Monte Carlo method
 * 
 * @param    n         Number of points used for the estimation
 * @return   double    Approched value of Pi
 */
double simPi(int n) {
    int i;
    double x, y;
    int inside = 0;
    double pi;

    for (i = 0; i < n; i++) {
        x = genrand_real1();
        y = genrand_real1();

        if (x*x + y*y <= 1)
            inside++;
    }

    pi = ((double) inside / (double) n) * 4;

    return pi;
}

/**
 * @brief              Function to calculate the mean of an array
 * 
 * @param    data      Array to calculate the mean from
 * @param    size      Size of the array
 * @return   double    Mean of the array
 */
double mean(double data[], int size) {
    double sum = 0.0;
    for (int i = 0; i < size; i++) {
        sum += data[i];
    }
    return sum / size;
}

/**
 * @brief              Function to calculate the standard deviation of an array
 * 
 * @param    data      Array to calculate the mean from
 * @param    size      Size of the array
 * @return   double    Standard deviation of the array
 */
double standardDeviation(double data[], int size) {
    double meanVal = mean(data, size);
    double sum = 0.0;
    for (int i = 0; i < size; i++) {
        sum += (data[i] - meanVal) * (data[i] - meanVal);
    }
    return sqrt(sum / (size - 1));
}

int main() {
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    // printf("Estimated pi with n=%d : %f\n", 1000, simPi(1000));
    // printf("Estimated pi with n=%d : %f\n", 1000000, simPi(1000000));
    // printf("Estimated pi with n=%d : %f\n", 1000000000, simPi(1000000000));

    int i, j;
    int n = 10;
    int v[] = { 1000, 1000000, 1000000000 };
    double *result = (double*) malloc(sizeof(double) * n);
    double meanPi;

    for (i = 0; i < 3; i++) {
        meanPi = 0;
        for (j = 0; j < n; j++)
            result[j] = simPi(v[i]);

        meanPi = mean(result, n);

        printf("Estimated pi with n=%d, %d drawings : %f (absolute error: %f)\n", v[i], n, meanPi, fabs(meanPi - M_PI));
    }

    double stddevPi = standardDeviation(result, n);

    printf("Standard deviation = %lf\n", stddevPi);


    double t_value = 2.042;  // from Student's t-table
    double marginOfError = t_value * stddevPi / sqrt(n);

    printf("95%% Confidence Interval = [%lf, %lf]\n", meanPi - marginOfError, meanPi + marginOfError);
    

    return 0;
}